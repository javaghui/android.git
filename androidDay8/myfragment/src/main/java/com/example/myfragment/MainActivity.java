package com.example.myfragment;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.ViewPager;

import android.os.Bundle;

import java.util.ArrayList;
import java.util.List;

/**
 * viewPager + Fragment
 */
public class MainActivity extends AppCompatActivity {

    private ViewPager viewPager;

    // 定义 viewpager 中的页面
    private List<Fragment> fragmentList = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        viewPager = findViewById(R.id.viewpage);

        //获取视图 界面
        fragmentList.add(new HomeFragment());
        fragmentList.add(new TypeFragment());

        // 将视图界面添加到viewPage 继承 ViewGroup
        // 往容器添加视图 用适配器

        viewPager.setAdapter(fragmentPagerAdapter);
    }
    FragmentPagerAdapter fragmentPagerAdapter = new FragmentPagerAdapter(getSupportFragmentManager()) {
        @NonNull
        @Override
        public Fragment getItem(int position) {
            return fragmentList.get(position);
        }

        @Override
        public int getCount() {
            return fragmentList.size();
        }
    };

}