package com.example.fragmentdemo;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


public class TwoFragment extends Fragment {
    private TextView textView;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_two, container, false);
        //获取数据
        String name = getArguments().getString("name");
        textView = view.findViewById(R.id.tv_get_data);
        textView.setText(name);

        return view;
    }

    // 给主页面传递参数
    public void setPassingData(Myinterface myinterface){
        String name = "周芷若";
        myinterface.getResult(name);
    }
    //定义一个接口
    public interface Myinterface{
        void getResult(String data);
    }
}