package com.example.fragmentdemo;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.os.Bundle;
import android.widget.LinearLayout;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private TextView textView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        textView = findViewById(R.id.tv_get_result);

        TwoFragment twoFragment = new TwoFragment();
        //数据传递
        Bundle bundle = new Bundle();
        bundle.putString("name","张三丰");
        twoFragment.setArguments(bundle);

        //接收TwoFragment 传递过来的参数
        twoFragment.setPassingData(new TwoFragment.Myinterface() {
            @Override
            public void getResult(String data) {
                textView.setText(data);
            }
        });

        //动态添加Fragment
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        // 通过id , 把 twoFragment 添加到LinearLayout布局中
        fragmentTransaction.add(R.id.linear_layout,twoFragment);
        // 替换
//        fragmentTransaction.replace(R.id.linear_layout,twoFragment);

        //删除
//        fragmentTransaction.remove(twoFragment);
        // 添加到回退栈  返时重新创建fragment
       fragmentTransaction.addToBackStack(null);

        //提交
        fragmentTransaction.commit();
    }
}