package com.example.mygpstest;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.location.LocationManager;
import android.os.Bundle;
import android.widget.TextView;

public class HomeActivity extends AppCompatActivity {

    private TextView textView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        textView = findViewById(R.id.tv_result);

        Intent intent = getIntent();
        //获取是否进入指定区域 entering
        boolean isEnter = intent.getBooleanExtra(LocationManager.KEY_PROXIMITY_ENTERING,false);


        if(isEnter){
            textView.setText("您已经进入指定区域");

        }else {
            textView.setText("警告，您不在指定区域内！！！");



        }
    }
}