package com.example.myhandler;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.health.TimerStat;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Handler 简单应用
 * 主线程才能处理界面UI
 * */
public class MainActivity extends AppCompatActivity {

    // 日志标记
    public  static final String TAG = MainActivity.class.getName();
    private TextView textView;
    private Button button,btnB;
    int data = 0;
    private Handler handler = new Handler(){
        @Override
        public void handleMessage(@NonNull Message msg) {
            // 这里是主线 所以可以操作ui
            Log.e(TAG,"-----------handleMessage------------");
            Log.e(TAG,"当前线程--ThreadID---"+Thread.currentThread().getId()+"--ThreadName---"+Thread.currentThread().getName());
            Log.e(TAG,"主线程----ThreadID-"+getMainLooper().getThread().getId()+"----ThreadName"+getMainLooper().getThread().getName());
            switch (msg.what){
                case 0x11:
                    data ++;
                    textView.setText(String.valueOf(data));
                    break;
            }

        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Log.e(TAG, "-----------onCreate------------");
        Log.e(TAG, "当前线程--ThreadID---" + Thread.currentThread().getId() + "--ThreadName---" + Thread.currentThread().getName());
        Log.e(TAG, "主线程----ThreadID-" + getMainLooper().getThread().getId() + "----ThreadName" + getMainLooper().getThread().getName());

        textView = findViewById(R.id.tv_test);
        button = findViewById(R.id.btn_start);
        btnB = findViewById(R.id.btn_startB);

        //发消息
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e(TAG, "当前线程onClick--ThreadID---" + Thread.currentThread().getId() + "--ThreadName---" + Thread.currentThread().getName());

                //每隔一秒发个消息
                Timer timer = new Timer();
                timer.schedule(new TimerTask() {
                    @Override
                    public void run() {
                        // 这里面是子线程 不可操作ui  会报错
//                        textView.setText("error");


                        Log.e(TAG, "-----------run------------");
                        Log.e(TAG, "当前线程--ThreadID---" + Thread.currentThread().getId() + "--ThreadName---" + Thread.currentThread().getName());
                        Log.e(TAG, "主线程----ThreadID-" + getMainLooper().getThread().getId() + "----ThreadName" + getMainLooper().getThread().getName());

//                        handler.sendEmptyMessage(0x11);
                        handler.sendEmptyMessage(0x222);

                    }
                }, 0, 1000);

            }
        });

        //子线程处理主线的UI
        btnB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new Thread(){

                    @Override
                    public void run() {
                        Log.e(TAG, "-----------Thread--run------------");
                        Log.e(TAG, "当前线程--ThreadID---" + Thread.currentThread().getId() + "--ThreadName---" + Thread.currentThread().getName());

                        Looper.prepare();

                        //子线程中的 handle
                        handler = new Handler(){
                            @Override
                            public void handleMessage(@NonNull Message msg) {
                                Log.e(TAG, "-----------Thread----handleMessage----------");
                                Log.e(TAG, "当前线程--ThreadID---" + Thread.currentThread().getId() + "--ThreadName---" + Thread.currentThread().getName());

                                if(msg.what == 0x222){
                                    data++;
                                    // 修改主线程中的UI
                                    MainActivity.this.runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            Log.e(TAG, "-----------Thread----runOnUiThread----------");
                                            Log.e(TAG, "当前线程--ThreadID---" + Thread.currentThread().getId() + "--ThreadName---" + Thread.currentThread().getName());

                                            //主线程
                                            textView.setText(data+"");
                                        }
                                    });
                                }
                            }
                        };

                        Looper.loop();
                    }

                }.start();

//                new Thread(() ->{
//
//                    //子线程中的 handle
//                    handler = new Handler(){
//                        @Override
//                        public void handleMessage(@NonNull Message msg) {
//                            if(msg.what == 0x222){
//                                data++;
//                                // 修改主线程中的UI
//                                MainActivity.this.runOnUiThread(new Runnable() {
//                                    @Override
//                                    public void run() {
//                                        //主线程
//                                        textView.setText(data+"");
//                                    }
//                                });
//                            }
//                        }
//                    };
//
//
//
//                }).start();


            }
        });

    }
}