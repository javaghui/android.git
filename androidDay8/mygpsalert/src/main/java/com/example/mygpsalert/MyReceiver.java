package com.example.mygpsalert;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.location.LocationManager;
import android.os.Looper;
import android.view.LayoutInflater;
import android.widget.Toast;

public class MyReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        //获取是否进入指定区域 entering
        boolean isEnter = intent.getBooleanExtra(LocationManager.KEY_PROXIMITY_ENTERING,false);
        Toast.makeText(context, "isEnter"+isEnter, Toast.LENGTH_SHORT).show();

        if(isEnter){
            Toast.makeText(context, "您已经进入指定区域", Toast.LENGTH_SHORT).show();

        }else {
            Toast.makeText(context, "警告，您不在指定区域内！！！", Toast.LENGTH_SHORT).show();

        }
    }

}