package com.example.myappstart;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    //图片打开资源
    private int[] onImages = new int[]{
            R.drawable.color_home,
            R.drawable.color_classification,
            R.drawable.color_discover,
            R.drawable.color_cart,
            R.drawable.color_author,
    };

    //图片关闭资源
    private int[] offImages = new int[]{
            R.drawable.home_bg,
            R.drawable.classification_bg,
            R.drawable.discover_bg,
            R.drawable.cart_bg,
            R.drawable.author_bg,
    };

    //图片按钮对象
    private final int[] btnImages = new int[]{
            R.id.img_home,
            R.id.img_classification,
            R.id.img_discover,
            R.id.img_cart,
            R.id.img_author,
    };

    //文本
    private int[] texts =new int[]{
            R.id.tv_home,
            R.id.tv_classification,
            R.id.tv_discover,
            R.id.tv_cart,
            R.id.tv_author,
    };

    //图片对象
    private ImageView[] imageViews;
    //文字
    private TextView[] textViews;
    //默认选中
    private ImageView homeView;
    private TextView homText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        imageViews = new ImageView[btnImages.length];
        textViews = new TextView[texts.length];

        for(int i =0 ; i < btnImages.length; i++){
            //图片
            imageViews[i] =findViewById(btnImages[i]);
            textViews[i] = findViewById(texts[i]);
            //事件
            imageViews[i].setOnClickListener(onClickListener);
        }

        //默认选中
        homeView = findViewById(R.id.img_home);
        homeView.setImageResource(R.drawable.color_home);
        homText = findViewById(R.id.tv_home);
        homText.setTextSize(14);
        homText.setTextColor(0xffff5000);
        homText.setTypeface(null, Typeface.BOLD);

    }

    private View.OnClickListener onClickListener = new View.OnClickListener() {


        @Override
        public void onClick(View v) {

            //先让所有图片变成关闭状态   文字恢复
            for(int i = 0 ; i < btnImages.length;i++){
                imageViews[i].setImageResource(offImages[i]);

                textViews[i].setTextSize(12);
                textViews[i].setTextColor(0xFFFFFFFF);
                textViews[i].setTypeface(null, Typeface.NORMAL);
            }
            //点击了哪个让图片选中  文字颜色改变 文字大小改边
            if (v.getId() == R.id.img_home) {
                ((ImageView) v).setImageResource(R.drawable.color_home);
//                imageViews[0].setImageResource(R.drawable.color_home);
                textViews[0].setTextColor(0xffff5000);
                textViews[0].setTextSize(14);
                textViews[0].setTypeface(null, Typeface.BOLD);
            } else if (v.getId() == R.id.img_classification) {
                ((ImageView) v).setImageResource(R.drawable.color_classification);
                textViews[1].setTextColor(0xffff5000);
                textViews[1].setTextSize(14);
                textViews[1].setTypeface(null, Typeface.BOLD);
            }else if (v.getId() == R.id.img_discover) {
                ((ImageView) v).setImageResource(R.drawable.color_discover);
                textViews[2].setTextColor(0xffff5000);
                textViews[2].setTextSize(14);
                textViews[2].setTypeface(null, Typeface.BOLD);
            }else if (v.getId() == R.id.img_cart) {
                ((ImageView) v).setImageResource(R.drawable.color_cart);
                textViews[3].setTextColor(0xffff5000);
                textViews[3].setTextSize(14);
                textViews[3].setTypeface(null, Typeface.BOLD);
            }else if (v.getId() == R.id.img_author) {
                ((ImageView) v).setImageResource(R.drawable.color_author);
                textViews[4].setTextColor(0xffff5000);
                textViews[4].setTextSize(14);
                textViews[4].setTypeface(null, Typeface.BOLD);
            }
        }
    };
}