package com.example.mysharedpreferences;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private Button btnCommit,btnGetdata;
    private EditText editText;
    private TextView textView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnCommit = findViewById(R.id.btn_submit);
        btnGetdata = findViewById(R.id.btn_get_data);
        editText = findViewById(R.id.et_content);
        textView = findViewById(R.id.tv_see);

        btnCommit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /** 开始保存数据
                 * xml文件名
                 * 文件访问模式
                 * MODE_PRIVATE         私有模式，只能本应用程序操作，会替换内容
                 *
                 * MODE_WORLD_READABLE          当前文件可以被其它应用读取
                 *
                 * MODE_WORLD_WRITEABLE          当前文件可以被其它应用写入
                 * */
                SharedPreferences sp=getSharedPreferences("data_xml", MODE_PRIVATE);
                /** 1.获取一个编辑对象 */
                SharedPreferences.Editor editor =sp.edit();
                /** 2.写数据保存 */
                String data = editText.getText().toString();
                editor.putString("textContent",data);
                /** 3.提交编辑 */
                editor.commit();

                Toast.makeText(getApplication(),"保存成功", Toast.LENGTH_SHORT).show();

            }
        });

        //读取xml文件
        btnGetdata.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /** 判断之前是否保存了数据 */
                /** 获取SharedPreferences如果之前已经存在就直接获取 反之 创建一个新的文件对象  */
                SharedPreferences sp = getSharedPreferences("data_xml",MODE_PRIVATE);
                /**
                 * sp.getString("textSizeData","默认值")
                 * 去文件对象中的值
                 * 如果值不存在 返回默认值 “默认值”
                 * */
                textView.setText(sp.getString("textContent","默认值"));

            }
        });
    }
}