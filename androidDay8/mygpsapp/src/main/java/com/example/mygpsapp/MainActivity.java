package com.example.mygpsapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

public class MainActivity extends AppCompatActivity {
    private Button button, btnGetData;

    //系统位置管理对象
    private LocationManager locationManager;

    private TextView textView;
    private EditText editText;
    private ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        button = findViewById(R.id.btn_get_provider);
        textView = findViewById(R.id.tv_see);
        listView = findViewById(R.id.list_view);
        btnGetData = findViewById(R.id.btn_get_data);
        editText = findViewById(R.id.et_content);

        //1.获取系统 位置管理对象  LocationManager
        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        // 2. 获取所有设备名字
        List<String> providerName = locationManager.getAllProviders();
        //2.1获取指定设备 gps
//        LocationProvider gpsProvider = locationManager.getProvider(LocationManager.GPS_PROVIDER);

        //3 把数据放到listView 中显示
        /**3个
         * passive: 代码表示：LocationManager.PASSIVE_PROVIDER
         * gps:  代码表示：LocationManager.GPS_PROVIDER
         * network: 网络获取定位信息  LocationManager.NETWORK_PROVIDER
         * */
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(this, R.layout.list_layout, providerName);
        listView.setAdapter(arrayAdapter);


        //从6.0系统开始,需要动态获取权限
        int permissionCheck = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION);
        if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 0);
        }

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Criteria 过滤 找到 定位设备
                //1.获取位置管理对象  LocationManager
                LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

                //2. 创建 Criteria 过滤条件
                Criteria criteria = new Criteria();

                //要求设备是免费的
                criteria.setCostAllowed(false);
                // 要求能提供高精度信息
                criteria.setAltitudeRequired(true);
                // 要求能提供反方向信息
                criteria.setBearingRequired(true);
                //   设置精度               标准不限
//                criteria.setAccuracy(Criteria.NO_REQUIREMENT);

                // 设置功率要求  电量               标准不限
//                criteria.setPowerRequirement(Criteria.NO_REQUIREMENT);

                //获取最佳提供商
                List<String> datas = locationManager.getAllProviders();

                textView.setText(datas.toString());
            }

        });

        //获取定位信息事件
        btnGetData.setOnClickListener(new View.OnClickListener() {
           @SuppressWarnings("all") //警告过滤
            @Override
            public void onClick(View v) {
                try {
                    //1.获取系统 位置管理对象  LocationManager
                    locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

                    //2.从GPS 获取最近定位信息
                    //获取到位置相关信息

                    Location location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);

                    //把信息设置到文本框中
                    updatView(location);

                    //设置每3秒 获取一次Gps 定位信息
                    locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 3000, 8, new LocationListener() {
                        @Override
                        public void onLocationChanged(@NonNull Location location) {
                            //当GPS 位置发生改变时 执行方法
                            updatView(location);
                        }

                        @Override
                        public void onProviderDisabled(@NonNull String provider) {
                            //禁用时 执行方法
                            updatView(null);
                        }

                        @Override
                        public void onProviderEnabled(@NonNull String provider) {
                            // 可以用时 执行方法

                            updatView(locationManager.getLastKnownLocation(provider));
                        }


                    });


                } catch (Throwable e) {
                    e.printStackTrace();
                }


            }

        });


    }
    //把信息设置到文本框中
    public void updatView(Location location) {
        if(location != null){
            StringBuilder cont = new StringBuilder();
            cont.append("实时定位信息：\n");
            cont.append("经度：");
            cont.append(location.getLongitude());
            cont.append("\n纬度：");
            cont.append(location.getLatitude());
            cont.append("\n高度：");
            cont.append(location.getAltitude());
            cont.append("\n速度：");
            cont.append(location.getSpeed());
            cont.append("\n方向：");
            cont.append(location.getBearing());
            editText.setText(cont.toString());

        }else {
            editText.setText("没有开启定位信息！");
        }
    }
    //请求权限结果
//    ACCESS_FINE_LOCATION  访问精细定位
//    ACCESS_COARSE_LOCATION 访问粗略定位
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case 0:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(MainActivity.this, "访问精细定位权限授权成功", Toast.LENGTH_SHORT).show();

                    //从6.0系统开始,需要动态获取权限
                    int permissionCheck = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION);
                    if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, 1);
                    }
                }
                break;
            case 1:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(MainActivity.this, "访问粗略定位权限授权成功", Toast.LENGTH_SHORT).show();
                }
                break;
            default:

                break;
        }

    }
}