package com.example.mysqlite.dto;

public class Student {
    public Long id;
    public String name;
    public String sex;
    public int age;
    public String clazz;

    public String creatDate;

    //头像
    public byte[] logoHead;
    @Override
    public String toString() {
        return "Student{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", sex='" + sex + '\'' +
                ", age=" + age +
                ", clazz='" + clazz + '\'' +
                ", creatDate='" + creatDate + '\'' +
                '}';
    }
}
