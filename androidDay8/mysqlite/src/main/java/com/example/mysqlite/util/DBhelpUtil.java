package com.example.mysqlite.util;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.Nullable;

public class DBhelpUtil extends SQLiteOpenHelper {

    /**数据库名字*/
    public static final String DB_NAME = "studentDB";

    /**学生表字段信息*/
    public static final String TABLE_NAME = "tb_student";
    public static final String TB_NAME = "name";
    public static final String TB_SEX = "sex";
    public static final String TB_AGE = "age";
    public static final String TB_CLAZZ = "clazz";
    public static final String TB_CREATEDATE = "createDate";

    /**数据版本号*/
//    public static final int DB_VERSION = 1;

    //数据版本升级
    public static final int DB_VERSION = 2;



    /**
     *
     * @param context   上下文
     * @param name      数据库名字
     * @param factory   游标工厂 null
     * @param version   自定义的数据库版本
     */
    public DBhelpUtil(@Nullable Context context, @Nullable String name, @Nullable SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    //数据库第一次创建时被调用
    @Override
    public void onCreate(SQLiteDatabase db) {
        //初始化 第一次 创建数据库
        StringBuilder sql = new StringBuilder();



        sql.append(" create table tb_student(");
        sql.append(" id integer primary key,  ");
        sql.append(" name varchar(20),");
        sql.append(" sex varchar(2),");
        sql.append(" age varchar(20),");
        sql.append(" clazz varchar(20),");
        sql.append(" createDate varchar(23) )");


//        Log.e("TAG","------"+sql.toString());

        //执行sql
        db.execSQL(sql.toString());
    }

    //版本号发生改变时调用
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        //更新数据库 插入字段
        String sql = "alter table tb_student add logoHead varchar(200)";

        db.execSQL(sql);

    }
}
